<!-- 
v1.0.0 - Fri, 17 Aug 2018 18:11:00 PST
---------------------------------------

- Update to react-async-script 1.0 [(#94)](https://github.com/dozoisch/react-google-recaptcha/pull/94)
- Add on Error [(#97)](https://github.com/dozoisch/react-google-recaptcha/pull/97)
- Update build tools
  - Node 8 [(#95)](https://github.com/dozoisch/react-google-recaptcha/pull/95)
  - Jest [(#95)](https://github.com/dozoisch/react-google-recaptcha/pull/95)
  - Eslint/Prettier [(#96)](https://github.com/dozoisch/react-google-recaptcha/pull/96)



v0.14.0 - Sun, 29 Jul 2018 19:20:03 GMT
---------------------------------------

- [701695b](../../commit/701695b) [changed] dynamic url creation to allow language change



v0.5.0 - Thu, 15 Oct 2015 21:38:43 GMT
--------------------------------------

- [d217dd1](../../commit/d217dd1) [changed] updated all deps
- [fc3350a](../../commit/fc3350a) [added] mt-changelog and release-script
- [dfa9bf2](../../commit/dfa9bf2) [added] doc for react 0.14 + old 0.13



## 0.3.1
- Added babel runtime to deps
- [#1] Removed unused use strict
- Bump deps

## 0.3.0
- Can now uses the recaptcha functions `getValue` `reset` directly from Wrapper without `getComponent` first.
- bump deps -->

## 0.0.1
- Initial commit of component
